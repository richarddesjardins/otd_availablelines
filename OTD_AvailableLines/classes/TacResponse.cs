﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTD_AvailableLines.classes
{
    public class TacResponse
    {
        public bool Success { get; set; }
        public string TAC { get; set; }
        public int IdleTrunks { get; set; }
        public int UsedTrunks { get; set; }
    }
}
