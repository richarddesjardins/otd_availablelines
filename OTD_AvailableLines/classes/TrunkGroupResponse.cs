﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTD_AvailableLines.classes
{
    public class TrunkGroupResponse
    {
        public List<string> Group_Number { get; set; }
        public List<string> TAC { get; set; }
        public List<string> Group_Type { get; set; }
        public List<string> Group_Name { get; set; }
        public List<string> Total_Administered_Members { get; set; }
        public List<string> TN { get; set; }
        public List<string> COR { get; set; }
        public List<string> CDR { get; set; }
        public List<string> Measured { get; set; }
        public List<string> Outgoing_Display { get; set; }
        public List<string> Queue_Length { get; set; }
        public List<string> Number_of_Members { get; set; }
        public List<string> Service_Type { get; set; }
        public List<string> Carrier_Medium { get; set; }
        public List<string> Native_Name_1 { get; set; }
        public List<string> Native_Name_2 { get; set; }
        public List<string> Native_Name_3 { get; set; }
        public List<string> Native_Name_4 { get; set; }
        public List<string> Native_Name_5 { get; set; }
        public List<string> Native_Name_Scripts { get; set; }
    }
}
