//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OTD_AvailableLines
{
    using System;
    using System.Collections.Generic;
    
    public partial class TrunkGroup
    {
        public int TrunkGroupID { get; set; }
        public int SwitchID { get; set; }
        public Nullable<int> TenantID { get; set; }
        public int GroupNumber { get; set; }
        public string TAC { get; set; }
        public string GroupType { get; set; }
        public string GroupName { get; set; }
        public bool IncludeInventory { get; set; }
        public bool Active { get; set; }
        public Nullable<System.DateTime> DateCreated { get; set; }
    
        public virtual Switch Switch { get; set; }
        public virtual Tenant Tenant { get; set; }
    }
}
