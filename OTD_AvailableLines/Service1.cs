﻿using Newtonsoft.Json;
using OTD_AvailableLines.classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OTD_AvailableLines
{
    public partial class Service1 : ServiceBase
    {
        Thread t;
        Thread t2;
        Thread t3;
        string conStr;
        bool _firstLaunch;

        const string p1 = "Update DialogicSnapshop Process";
        const string p2 = "Maintain Trunk Group Process";
        const string p3 = "Run Outbound Leads Process";

        public Service1()
        {
            InitializeComponent();
            _firstLaunch = true;
            conStr = System.Configuration.ConfigurationManager.ConnectionStrings["CLUSTER"].ConnectionString;
        }

        protected override void OnStart(string[] args)
        {
            _firstLaunch = true;

            ThreadStart ts2 = new ThreadStart(Process2);
            t2 = new Thread(ts2);
            t2.Start();

            ThreadStart ts3 = new ThreadStart(Process3);
            t3 = new Thread(ts3);
            t3.Start();
            
        }

        protected void Process3()
        {
            Thread.Sleep(1000 * 10);
            LogMessage("Information", p3 + " started", "P3");
            try
            {
                using (SqlConnection con = new SqlConnection(conStr))
                {
                    using (SqlCommand cmd = new SqlCommand("uspRunOutboundLeadsJob", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch(Exception ex)
            {
                LogMessage("Exception", getExceptionText(ex),"P3");
            }

            Process3();

        }
        protected async void Process2()
        {
            LogMessage("Information", "Maintain Trunk Group table process started","P2");
            // this will maintain the TrunkGroup Lokup table
            if (_firstLaunch == true)
            {
                Thread.Sleep(30 * 1000);// Sleep for 20 seconds in case we want to attached to thread.
            }
            //Do some work
            ///////////////////////////////////////

            using (DialerAdminEntities context = new DialerAdminEntities())
            {
               var switches = context.Switches.Where(s => s.Active == true).ToList();

                foreach (var s in switches)
                {

                    var tg = context.TrunkGroups.Where(w => w.SwitchID == s.SwitchID);
                    var existingTACList = tg.Select(t => t.TAC).ToList();

                    TrunkGroupResponse groupResp = new TrunkGroupResponse();
                    HttpClient client = new HttpClient();
                    string APIServer = string.Empty;
                    if (s.FailOverActive != null && s.FailOverActive.Value)
                    {
                        APIServer = s.SecondaryAPIServerIP;
                    }
                    else
                    {
                        APIServer = s.APIServerIP;
                    }

                    try
                    {
                        var task1 = await client.GetAsync(APIServer + "/TrunkGroup/List");
                        var jsonString1 = await task1.Content.ReadAsStringAsync();
                        groupResp = JsonConvert.DeserializeObject<TrunkGroupResponse>(jsonString1);

                        // Find Records that have been removed and mark them as inactive
                        var removed = tg.Where(w => !groupResp.TAC.Contains(w.TAC));
                        var added = groupResp.TAC.Where(w => !existingTACList.Contains(w));
                        var existing = groupResp.TAC.Where(w => existingTACList.Contains(w));
                        List<TrunkGroup> newTGs = new List<TrunkGroup>();
                        foreach(string tac in added)
                        {
                            // get index
                            int index = -1;
                            for (int i = 0; i < groupResp.TAC.Count(); i++)
                            {
                                if (tac == groupResp.TAC[i])
                                {
                                    index = i;
                                }
                            }
                            // build
                            TrunkGroup g = new TrunkGroup();
                            g.Active = true;
                            g.DateCreated = DateTime.Now;
                            g.GroupName = groupResp.Group_Name[index];
                            g.GroupNumber = int.Parse(groupResp.Group_Number[index]);
                            g.GroupType = groupResp.Group_Type[index];
                            g.IncludeInventory = false;
                            g.SwitchID = s.SwitchID;
                            g.TAC = tac;
                            g.TenantID = null;

                            newTGs.Add(g);
                        }

                        foreach (var g in removed)
                        {
                            g.Active = false;
                            g.IncludeInventory = false;
                        }

                        context.TrunkGroups.AddRange(newTGs);

                        // Check for updates
                        foreach(var tac in existing)
                        {
                            int index = -1;
                            for (int i = 0; i < groupResp.TAC.Count(); i++)
                            {
                                if (tac == groupResp.TAC[i])
                                {
                                    index = i;
                                }
                            }
                            // get record
                            var entity = tg.Where(w => w.TAC == tac).FirstOrDefault();
                            if (entity != null)
                            {
                                bool hasUpdate = false;
                                if (entity.GroupName != groupResp.Group_Name[index])
                                {
                                    entity.GroupName = groupResp.Group_Name[index];
                                    hasUpdate = true;
                                }

                                if (entity.GroupNumber != int.Parse(groupResp.Group_Number[index]))
                                {
                                    entity.GroupNumber = int.Parse(groupResp.Group_Number[index]);
                                    hasUpdate = true;

                                }

                                if (entity.GroupType != groupResp.Group_Type[index])
                                {
                                    entity.GroupType = groupResp.Group_Type[index];
                                    hasUpdate = true;
                                }

                                if (hasUpdate)
                                {
                            
                                    entity.IncludeInventory = false;
                                }

                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        LogMessage("Exception", getExceptionText(ex),"P2");
                    }


                    context.SaveChanges();
            }

                
            }
            ///////////////////////////////////////

            if (_firstLaunch == true)
            {
                ThreadStart ts = new ThreadStart(Process);
                t = new Thread(ts);
                t.Start();
                _firstLaunch = false;
            }
            LogMessage("Information", p2 + " completed","P2");
            Thread.Sleep(1000 * 60 * 60 * 24); // Chill for a day
            Process2();
        }

        protected async void Process()
        {

            LogMessage("Information", p1 + " started","P1");
            // Do work
            //////////////////////////////////////////////////////////


            using (DialerAdminEntities context = new DialerAdminEntities())
            {
                HttpClient client = new HttpClient();
                var tg = context.TrunkGroups.Where(w => w.IncludeInventory == true && w.Active == true && w.Tenant != null);
                var tenantList = tg.Select(t => t.TenantID).Distinct();
                var dSnap = context.DialogicSnapshots.ToList();


                foreach(var tID in tenantList)
                {
                    var tenantTrunkGroups = tg.Where(w => w.TenantID.Value == tID);
                    string APIUrl = string.Empty;
                    int totalLines = 0;
                    int linesInUse = 0;
                    int availableLines = 0;
                    bool hasErrors = false;
                    foreach (var dss in tenantTrunkGroups)
                    {

                        if (dss.Tenant != null)
                        {
                            if (dss.Tenant.Switch != null)
                            {
                                if (dss.Tenant.Switch.FailOverActive.Value)
                                {
                                    APIUrl = dss.Tenant.Switch.SecondaryAPIServerIP;
                                }
                                else
                                {
                                    APIUrl = dss.Tenant.Switch.APIServerIP;
                                }

                                try
                                {
                                    var task2 = await client.GetAsync(APIUrl + "/TrunkGroup/GetUsage/?TAC=" + dss.TAC);
                                    var jsonString2 = await task2.Content.ReadAsStringAsync();
                                    TacResponse tacResp = JsonConvert.DeserializeObject<TacResponse>(jsonString2);

                                    if (tacResp.Success)
                                    {
                                        totalLines += (tacResp.IdleTrunks + tacResp.UsedTrunks);
                                        linesInUse += tacResp.UsedTrunks;
                                        availableLines += tacResp.IdleTrunks;
                                    }
                                    else
                                    {
                                        LogMessage("Error", "TAC Response was not successful","P1");
                                        hasErrors = true;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogMessage("Exception", getExceptionText(ex),"P1");
                                    hasErrors = true;
                                }

                            }
                        }
                        var ds = dSnap.Where(w => w.TenantID == dss.TenantID).FirstOrDefault();

                        if (!hasErrors)
                        {
                            if (ds != null)
                            {
                                ds.TotalLines = totalLines;
                                ds.LinesInUse = linesInUse;
                                ds.AvailableLines = availableLines;
                            }
                            else
                            {
                                //insert
                                DialogicSnapshot newSS = new DialogicSnapshot();
                                //newSS.ActivityLookback = null;// Defualt = 15
                                newSS.TotalLines = totalLines;
                                newSS.LinesInUse = linesInUse;
                                newSS.AvailableLines = availableLines;

                                context.DialogicSnapshots.Add(newSS);
                            }
                        }
                    }
                }

                context.SaveChanges();
            }


            ///////////////////////////////////////////////////////////
            // Rest
            LogMessage("Information", "Update DialogicSnapshop Process completed","P1");
            Thread.Sleep(1000 * 3);
            Process();
            
        }

        protected override void OnStop()
        {
            LogMessage("Information","Service Stopped","Service");
        }

        protected void LogMessage(string LogType, string LogMessage,string process)
        {
            var location = System.Reflection.Assembly.GetEntryAssembly().Location;
            var directoryPath = Path.GetDirectoryName(location);
            try
            {
                using (Stream s = File.Open(directoryPath + "\\log_" + process + ".txt", FileMode.Append))
                {
                    using (StreamWriter sw = new StreamWriter(s))
                    {
                        sw.WriteLine(DateTime.Now.ToString("MM/dd/yy hh:mm:ss") + ": " + LogType + ": " + LogMessage);
                        sw.Flush();
                    }
                }
            }
            catch(Exception ex)
            {
                using (Stream s = File.Open(directoryPath + "\\log_" + process + "_" + DateTime.Now.ToString("yyyMMddhhmmss") + ".txt", FileMode.CreateNew))
                {
                    using (Stream s1 = File.Open(directoryPath + "\\log_" + process + ".txt", FileMode.Open))
                    {
                        using (StreamReader sr = new StreamReader(s1))
                        {
                            using (StreamWriter sw = new StreamWriter(s))
                            {
                                sw.WriteLine(sr.ReadLine());
                                sw.Flush();
                            }
                        }
                    }


                }

                using (Stream s = File.Open(directoryPath + "\\log_" + process + ".txt", FileMode.Truncate))
                {
                    using (StreamWriter sw = new StreamWriter(s))
                    {
                        sw.WriteLine(DateTime.Now.ToString("MM/dd/yy hh:mm:ss") + ": " + LogType + ": " + LogMessage);
                        sw.Flush();
                    }
                }
            }


        }

        protected string getExceptionText(Exception ex)
        {
            StringBuilder sbMessage = new StringBuilder();
            sbMessage.Append(ex.Message);
            if (ex.InnerException != null)
            {
                sbMessage.Append(" InnerException: " + ex.InnerException.Message);
            }

            sbMessage.Append(" Stack Trace:" + ex.StackTrace);

            return sbMessage.ToString();
        }


    }
}
